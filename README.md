**The OpenOximeter is currently in an early prototype stage. It is never safe to build your own medical equipment.**
**This project is open to encourage experts to work together to build the best and most adaptable solution.**

This breakout board was designed to replace the commercial breakout-boards if supply became short during the pandemic.

Some caveats to using this design:
- The dimensions for this board are smaller than the other boards used in the project - as such a clip redesign would be required to make use of this board.
- The board was designed for fabrication at a professional fab house, with requirements for through hole plating and solder resist.
- While it is possible to fabricate these boards by hand, the MAX chipsets have a very small footprint, meaning use of re-flow ovens is advised.

![PCB CAD Model](CAD_Model/BoardRender.PNG "CAD model of the breakout PCB")